/* Konfiguracja */

const contentElementId = 'content';
const scoreElementId = 'score';

const buttons = [
    [
        { value: "+", onClick: "add('+')", class: 'function' },
        { value: "-", onClick: "add('-')", class: 'function' },
        { value: "/", onClick: "add('/')", class: 'function' },
        { value: "x", onClick: "add('*')", class: 'function' }
    ],
    [
        { value: "7", onClick: "add(7)" },
        { value: "8", onClick: "add(8)" },
        { value: "9", onClick: "add(9)" },
        { value: "C", onClick: "clearScore()", class: 'function' }
    ],
    [
        { value: "4", onClick: "add(4)" },
        { value: "5", onClick: "add(5)" },
        { value: "6", onClick: "add(6)" },
        { value: "&nbsp;", onClick: "", class: 'disabled' }
    ],
    [
        { value: "1", onClick: "add(1)" },
        { value: "2", onClick: "add(2)" },
        { value: "3", onClick: "add(3)" },
        { value: "=", onClick: "score()", class: 'function' }
    ],
    [
        { value: "0", onClick: "add('0')", class: 'special' },
        { value: ".", onClick: "add('.')", class: 'special' },
        { value: "(", onClick: "add('(')", class: 'special' },
        { value: ")", onClick: "add(')')", class: 'special' }
    ]
];

window.setTimeout(() => {
    let content = '';
    content += '<input id="score" type="textview" name="score" value="0" readonly /><br />';
    buttons.forEach(function (item) {
        item.forEach(function (subItem) {
            content += '<button class="' + subItem.class + '" onClick="' + subItem.onClick + '">' + subItem.value + '</button>';
        });
        content += '<br />';
    });
    document.getElementById(contentElementId).innerHTML = content;
}, 100);

function add (param) {
    let textViewContent = document.getElementById(scoreElementId).value || "";
    textViewContent += param;
    document.getElementById(scoreElementId).value = textViewContent;
}

function clearScore() {
    document.getElementById(scoreElementId).value = '0';
}

function score() {
    const textViewContent = document.getElementById(scoreElementId).value || "";
    document.getElementById(scoreElementId).value = eval(textViewContent) || 0;
}